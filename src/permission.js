import router from "./router";
import store from "./store";
import { Message } from "element-ui";
import NProgress from "nprogress"; // progress bar
import "nprogress/nprogress.css"; // progress bar style
import { getToken } from "@/utils/auth"; // get token from cookie
import getPageTitle from "@/utils/get-page-title";

NProgress.configure({ showSpinner: false }); // NProgress Configuration

const whiteList = ["/login"]; // no redirect whitelist

router.beforeEach(async (to, from, next) => {
  // start progress bar
  NProgress.start();

  // set page title
  document.title = getPageTitle(to.meta.title);

  // determine whether the user has logged in
  const hasToken = getToken();
  // console.log("router.beforeEach  to.path:", to.path);
  if (hasToken) {
    if (to.path === "/login") {
      // if is logged in, redirect to the dashboard page
      next({ path: "/" });
      NProgress.done();
    } else {
      // determine whether the user has obtained his permission roles through getInfo
      const permission_routes = store.getters.permission_routes;
      if (permission_routes.length === 0) {
        // 判断当前用户是否已拉取完user_info信息
        store
          .dispatch("user/getIdRoutes", sessionStorage.getItem("userId"))
          .then((res) => {
            store
              .dispatch("permission/generateRoutes", res)
              .then((accessRoutes) => {
                // 根据roles权限生成可访问的路由表
                router.addRoutes(accessRoutes); // 动态添加可访问路由表
                // next({ ...to, replace: true }); // hack方法 确保addRoutes已完成
                next({ ...to, replace: true });
              });
          })
          .catch((err) => {
            store.dispatch("user/logout").then(() => {
              Message.error(err);
              next({ path: "/" });
            });
          });
        next();
      } else {
        next();
      }
    }
  } else {
    /* has no token*/

    if (whiteList.indexOf(to.path) !== -1) {
      // in the free login whitelist, go directly
      next();
    } else {
      // other pages that do not have permission to access are redirected to the login page.
      next(`/login?redirect=${to.path}`);
      NProgress.done();
    }
  }
});

router.afterEach(() => {
  // finish progress bar
  NProgress.done();
});
