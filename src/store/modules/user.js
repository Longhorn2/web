import { login, getIdRoutes } from '@/utils/api';
import { getToken, setToken, removeToken } from '@/utils/auth';
import router, { resetRouter } from '@/router';
import Layout from "@/layout";

const func = (datas, arr1) => {
  datas.forEach(val => {
    let temp = {
    };
    temp.path = val.router_path;
    temp.name = val.router_name;
    if (val.redirect) {
      temp.redirect = val.redirect;
    }
    if (val.meta) {
      temp.meta = {};
      temp.meta = meta;
    }
    if (val.menu_name) {
      temp.meta = {};
      temp.meta.title = val.menu_name;
      temp.meta.icon = val.icon ? val.icon : 'lock';
    }
    if (val.router_views != null && val.router_views === 'Layout') {
      temp.component = Layout;
    } else if (val.router_views != null && val.router_views) {
      temp.component = loadView(val.router_views);
    }
    // 判断是否为按钮,把按钮过滤掉
    if (val.type === 0) {
      arr1.push(temp);
    }

    if (val.children && val.children.length > 0) {
      temp.children = [];
      func(val.children, temp.children);
    }
  });
  // console.log('func处理后返回值 medatas',arr1)
};
const loadView = (view) => {
  if (view) {
    // webpack 还没有完全支持动态引入
    return (resolve) => require([`@/views${view}`], resolve);
  }
};
const state = {
  token: getToken(),
  email: '',
  userId: '',
  name: '',
  avatar: '',
  introduction: '',
  roles: [],
}

const mutations = {
  SET_TOKEN: (state, token) => {
    state.token = token;
  },
  SET_EMAIL: (state, email) => {
    state.email = email;
  },
  SET_USERID: (state, userId) => {
    state.userId = userId;
  },
  SET_INTRODUCTION: (state, introduction) => {
    state.introduction = introduction;
  },
  SET_NAME: (state, name) => {
    state.name = name;
  },
  SET_AVATAR: (state, avatar) => {
    state.avatar = avatar;
  },
  SET_ROLES: (state, roles) => {
    state.roles = roles;
  },
  SET_SIDBARRUTERS: (state, sidbarRuters) => {
    state.sidbarRuters = sidbarRuters
  },
}
const actions = {
  // user login
  login ({ commit, state }, userInfo) {
    const { email, pwd } = userInfo;
    return new Promise((resolve, reject) => {
      login({ email: email.trim(), pwd: pwd }).then(response => {
        const { data } = response;
        sessionStorage.setItem("userInfo", JSON.stringify(data.userInfo));
        commit('SET_NAME', data.userInfo.user_name);
        commit('SET_EMAIL', data.userInfo.email);
        commit('SET_AVATAR', data.userInfo.avatar);
        commit('SET_INTRODUCTION', data.userInfo.source);
        commit('SET_TOKEN', data.accessToken);
        setToken(data.accessToken);
        commit('SET_USERID', data.userInfo.id);
        sessionStorage.setItem(
          "userId",
          String(data.userInfo.id),
        );
        resolve();
      }).catch(error => {
        reject(error);
      });
    });
  },

  getIdRoutes ({ commit }, userId) {
    return new Promise((resolve, reject) => {
      getIdRoutes(userId).then(response => {
        const { data } = response;
        if (!data) {
          reject('Verification failed, please Login again.');
        }
        let filterRoute = [];
        func(data, filterRoute);
        commit('SET_SIDBARRUTERS', filterRoute);
        resolve(filterRoute);
      }).catch(error => {
        reject(error);
      });
    });
  },

  // user logout
  logout ({ commit }) {
    commit('SET_TOKEN', '');
    removeToken();
    sessionStorage.setItem("roles", '');
  },

  // remove token
  resetToken ({ commit }) {
    return new Promise(resolve => {
      commit('SET_TOKEN', '');
      commit('SET_ROLES', []);
      removeToken();
      resolve();
    });
  },

  // dynamically modify permissions
  async changeRoles ({ commit, dispatch }, role) {
    const token = role + '-token';

    commit('SET_TOKEN', token);
    setToken(token);

    const { roles } = await dispatch('getInfo');

    resetRouter();
    // generate accessible routes map based on roles
    const accessRoutes = await dispatch('permission/generateRoutes', roles, { root: true });
    // dynamically add accessible routes
    // router.addRoutes(accessRoutes)
    // reset visited views and cached views
    dispatch('tagsView/delAllViews', null, { root: true });
  }
};

export default {
  namespaced: true,
  state,
  mutations,
  actions
};
