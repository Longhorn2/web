// 下载文件重命名
import axios from 'axios'
const downloadFile = (furl, fname) => {
  let path = furl
  axios({
    method: 'get',
    responseType: 'blob', // 设置响应文件格式
    url: path
  }).then(({ data }) => {
    if (window.navigator.msSaveOrOpenBlob) {
      // 兼容IE10
      navigator.msSaveBlob(data, fname)
    } else {
      let url = null
      if (window.createObjectURL) { // basic
        url = window.createObjectURL(data)
      } else if (window.webkitURL) { // webkit or chrome
        try {
          url = window.webkitURL.createObjectURL(data)
        } catch (error) {
        }
      } else if (window.URL) { // mozilla(firefox)
        try {
          url = window.URL.createObjectURL(data)
        } catch (error) {
        }
      }

      console.log(url, 'urlurl')
      const a = document.createElement('a') // 创建a标签
      a.setAttribute('download', fname)// download属性
      a.setAttribute('href', url)// href链接
      a.click()// 自执行点击事件
    }
  })
}
export {
  downloadFile
}
