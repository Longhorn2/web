import request, { POST, GET } from "@/utils/request";
/**
 * 查询用户列表
 */

export function getUserlist(data) {
  return POST("/longhorn-web/sysUser/getUserInforList", data);
}
/**
 * 添加用户

 */
export function addUser(data) {
  return POST("/longhorn-web/sysUser/addUser", data);
}
/**
 * 更新用户

 */
export function updateUser(data) {
  return POST("/longhorn-web/sysUser/edit", data);
}
/**
 * 获取所有角色列表

 */
export function getAllList() {
  return GET("/longhorn-web/sysRole/getAllList", "");
}
/**
 * 获取组织列表

 */
export function getOrgList(userId) {
  return GET("/longhorn-web/sysOrg/getList/" + userId, "");
}
// export function grouplist () {
//   return GET('/longhorn-web/sysOrg/getList', "");
// }
/**
 * 设置组织菜单

 */
export function setRoleMenu(data) {
  return POST("/longhorn-web/sysUser/setUserRole", data);
}

export function getIdOrgs(id) {
  return GET("/longhorn-web/sysOrg/getUserOrgList/" + id, "");
}

export function setOrgMenu(data) {
  return POST("/longhorn-web/sysUser/setUserOrg", data);
}

export function getAllSitesName(data) {
  return POST("/longhorn-web/sysOrg/getStationListByOrg", data);
}

export function getIdSites(userId) {
  return GET("/longhorn-web/stationManagement/getAllList/" + userId, "");
}

export function setOrgPermission(data) {
  return POST("/longhorn-web/sysOrg/getStationListByOrg", data);
}

export function getDicValue(data) {
  return POST("/longhorn-web/sysDataType/getList", data);
}

export function dicdelete(id) {
  return GET("/longhorn-web/sysDataType/del/" + id, "");
}

export function dicAdd(data) {
  return POST("/longhorn-web/sysDataType/addSysDataType", data);
}

export function dicUpdate(data) {
  return POST("/longhorn-web/sysDataType/edit", data);
}

export function getListByCode(data) {
  return POST("/longhorn-web/sysDataType/getListByCode", data);
}

export function getDataTypeList(data) {
  return POST("/longhorn-web/sysDataType/getListByCode", data);
}

export function menuupdate(data) {
  return POST("/longhorn-web/sysMenu/updMenu", data);
}

export function menulist() {
  return GET("/longhorn-web/sysMenu/getList", "");
}

export function delMenu(id) {
  return GET("/longhorn-web/sysMenu/del/" + id, "");
}

export function menuadd(data) {
  return POST("/longhorn-web/sysMenu/addMenu", data);
}

export function grouplist(userId) {
  return GET("/longhorn-web/sysOrg/getList/" + userId, "");
}

export function deleteOrg(id) {
  return GET("/longhorn-web/sysOrg/del/" + id, "");
}

export function orgAdd(data) {
  return POST("/longhorn-web/sysOrg/addOrg", data);
}

export function orgUpdate(data) {
  return POST("/longhorn-web/sysOrg/edit", data);
}

/*******角色管理***********/

export function getRoutes() {
  return GET("/longhorn-web/sysRole/routes", "");
}

export function getRoles(data) {
  return POST("/longhorn-web/sysRole/getList", data);
}

export function addRole(data) {
  return POST("/longhorn-web/sysRole/addRole", data);
}

export function deleteRole(id) {
  return GET("/longhorn-web/sysRole/del/" + id, "");
}

export function updateRole(data) {
  return POST("/longhorn-web/sysRole/edit", data);
}

export function getUserIdRoles(userId) {
  return GET("/longhorn-web/sysUser/selfMenu/" + userId, "");
}

export function setSysRoleMenu(data) {
  return POST("/longhorn-web/sysRole/setRoleMenu", data);
}

export function getIdRoles(id) {
  return GET("/longhorn-web/sysRole/getMenuByRole/" + id, "");
}

/*********客户管理************/

export function getCustomerList(data) {
  return POST("/longhorn-web/customer/management/getCustomerList", data);
}

export function getUserAuthList() {
  return GET("/longhorn-web/sysUser/getUserAuthList", "");
}

export function getUserAuth(id, type) {
  return GET("/longhorn-web/sysUser/getUserAuth/" + id + "/" + type, "");
}

export function saveSysAuth(data) {
  return POST("/longhorn-web/sysUser/saveSysAuth", data);
}

export function addCustomerUser(data) {
  return POST("/longhorn-web/customer/management/addUser", data);
}

export function editUser(data) {
  return POST("/longhorn-web/customer/management/editUser", data);
}

export function setUserStatus(id, status) {
  return GET(
    "/longhorn-web/customer/management/setUserStatus/" + id + "/" + status,
    ""
  );
}

/*********告警************/

export function getHisAlarmList(data) {
  return POST("/longhorn-web/sys/alarm/getHisAlarmList", data);
}

export function getCustomerAllList(userId, customer_type) {
  return GET(
    "/longhorn-web/customer/management/getCustomerAllList/" +
      userId +
      "/" +
      customer_type,
    ""
  );
}

/*********告警配置************/

export function getSysAlarmConfigList(data) {
  return POST("/longhorn-web/sys/alarm/getSysAlarmConfigList", data);
}

export function addSysAlarmConfig(data) {
  return POST("/longhorn-web/sys/alarm/addSysAlarmConfig", data);
}

export function updSysAlarmConfig(data) {
  return POST("/longhorn-web/sys/alarm/updSysAlarmConfig", data);
}

export function delSysAlarmConfig(id) {
  return GET("/longhorn-web/sys/alarm/delSysAlarmConfig/" + id, "");
}

/*********桩管理************/

export function getPileManagement(data) {
  return POST("/longhorn-web/pileManagement/list", data);
}

export function addPileManagement(data) {
  return POST("/longhorn-web/pileManagement/add", data);
}

export function editPileManagement(data) {
  return POST("/longhorn-web/pileManagement/edit", data);
}

/*********站点管理************/

export function getManagement(data) {
  return POST("/longhorn-web/stationManagement/list", data);
}

export function addManagement(data) {
  return POST("/longhorn-web/stationManagement/add", data);
}

export function editManagement(data) {
  return POST("/longhorn-web/stationManagement/edit", data);
}

export function getListCountry(userId, id) {
  return GET("/longhorn-web/sysOrg/getOrgListById/" + userId + "/" + id, "");
}

export function getBrandOptions(userId, customer_type) {
  return GET(
    "/longhorn-web/customer/management/getCustomerAllList/" +
      userId +
      "/" +
      customer_type,
    ""
  );
}

export function getStationNameList(id) {
  return GET("/longhorn-web/stationManagement/getAllList/" + id, "");
}

export function qeuryStation(data) {
  return POST("/longhorn-web/stationManagement/list", data);
}

export function addStation(data) {
  return POST("/longhorn-web/stationManagement/add", data);
}
export function addImg(data) {
  return POST(
    "/longhorn-web/stationManagement/uploadFileByName?fileName=stationimg",
    data
  );
}

export function getStationDetails(id) {
  return GET("/longhorn-web/stationManagement/getStationDetails/" + id, "");
}

/*********用户登录************/

export function login(data) {
  return POST("/longhorn-web/sysUser/login", data);
}
// 根据id获取拥有的菜单

export function getIdRoutes(userId) {
  return GET("/longhorn-web/sysUser/selfMenu/" + userId, "");
}
//查询充电订单
export function getChargeOrderList(data) {
  return POST("/longhorn-web/sysChargeOrder/getChargeOrderList", data);
}
// 修改个人信息
export function changeInformation(userName, contact_phone, contact_phone_code) {
  return GET("/longhorn-web/sysUser/changeInformation/" + userName + "/" + contact_phone, "/" + contact_phone_code, "");
}
// 修改密码
