/**
 * @param {string} time
 * @returns {yyyy-MM-dd}
 */
export function handleDate(date) {
  // 使用原生 JavaScript 格式化日期。例如：date = Tue Jul 11 2023 00:00:00 GMT+0800 (中国标准时间)
  const selectedDate = new Date(date);
  const year = selectedDate.getFullYear();
  const month = String(selectedDate.getMonth() + 1).padStart(2, "0");
  const day = String(selectedDate.getDate()).padStart(2, "0");
  const formattedDate = `${year}-${month}-${day}`;
  // 这里的输出格式如： 2023-07-26
  return formattedDate;
}
//将时间戳解析为日期字符串
export function parseTimeStamp(time) {
  
  // item.create_time = new Date(item.create_time).toISOString();
  // 输出时间类似 "2023-07-11 10:30:00" 的字符串
  const dateObj = new Date(time);
  const year = dateObj.getFullYear();
  const month = dateObj.getMonth() + 1; // 月份从0开始，需要加1
  const day = dateObj.getDate();
  const hours = dateObj.getHours();
  const minutes = dateObj.getMinutes();
  const seconds = dateObj.getSeconds();
  const formattedDateTime =
    year +
    "-" +
    String(month).padStart(2, "0") +
    "-" +
    String(day).padStart(2, "0") +
    " " +
    String(hours).padStart(2, "0") +
    ":" +
    String(minutes).padStart(2, "0") +
    ":" +
    String(seconds).padStart(2, "0");
  return formattedDateTime;
}