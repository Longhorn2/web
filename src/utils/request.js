import axios from "axios";
import { MessageBox, Message } from "element-ui";
import store from "@/store";
import { getToken } from "@/utils/auth";

// create an axios instance
const http = axios.create({
  headers: {
    language: "zh",
    "Content-Type": "application/json;charset=UTF-8",
    Authorization: "bearer " + getToken(),
  },
  baseURL: process.env.VUE_APP_BASE_API, // url = base url + request url
  // withCredentials: true, // send cookies when cross-domain requests
  timeout: 5000, // request timeout
  baseURL: "/",
});

// request interceptor
http.interceptors.request.use(
  (config) => {
    // do something before request is sent

    if (store.getters.token) {
      // let each request carry token
      // ['X-Token'] is a custom headers key
      // please modify it according to the actual situation
      config.headers.Authorization = getToken();
    }
    if (store.getters.language == "zh") {
      config.headers.language = "ZH";
    } else if (store.getters.language == "en") {
      config.headers.language = "EN";
    }
    return config;
  },
  (error) => {
    // do something with request error
    return Promise.reject(error);
  }
);

// response interceptor
http.interceptors.response.use(
  /**
   * If you want to get http information such as headers or status
   * Please return  response => response
   */

  /**
   * Determine the request status by custom code
   * Here is just an example
   * You can also judge the status by HTTP Status Code
   */
  (response) => {
    const res = response.data;

    // if the custom code is not 0, it is judged as an error.
    if (res.code !== 0 && res.code !== 200) {
      Message({
        message: res.errMsg || "Error",
        type: "error",
        duration: 5 * 1000,
      });

      if (res.code === 401 || res.code === 1002 || res.code === 1005) {
        // to re-login
        MessageBox.confirm(
          "You have been logged out, you can cancel to stay on this page, or log in again",
          "Confirm logout",
          {
            confirmButtonText: "Re-Login",
            cancelButtonText: "Cancel",
            type: "warning",
          }
        ).then(() => {
          store.dispatch("user/logout");
        });
      }
      return Promise.reject(new Error(res.message || "Error"));
    } else {
      return res;
    }
  },
  (error) => {
    Message({
      message: error.message,
      type: "error",
      duration: 5 * 1000,
    });
    return Promise.reject(error);
  }
);
export function GET(url, data = {}) {
  // console.log(store.getters.language);
  return new Promise((resolve, reject) => {
    http
      .get(url)
      // , {
      //   params: data
      // }
      .then((response) => {
        resolve(response);
      })
      .catch((err) => {
        reject(err);
      });
  });
}
export function POST(url, data = {}) {
  return new Promise((resolve, reject) => {
    http.post(url, data).then(
      (res) => {
        resolve(res);
      },
      (err) => reject(err)
    );
  });
}

export function request(methed, url, data = {}, headers, responseType = "") {
  return new Promise((resolve, reject) => {
    http({
      method: methed || "post",
      url: url,
      params: methed === "get" ? data : "",
      data: methed !== "get" ? data : "",
      headers: headers || {
        "Content-Type": "application/json",
      },
      responseType: responseType,
    })
      .then((response) => {
        // 对接口错误码做处理
        resolve(response.data);
      })
      .catch((err) => {
        reject(err);
      });
  });
}
export default http;
